$(".ie-warn").waitUntilExists(function () {
  var message = "For optimum Web performance, use Internet Explorer 9 or later." +
    " We no longer support Internet Explorer 8";

  $(".ie-warn").html("<!--[if lt IE 9]>" + message + "<![endif]-->");
});
