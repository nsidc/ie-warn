This script can be included in your project to display a warning message to users visiting your page with Internet Explorer 8. All you have to do is create an HTML element with the class "ie-warn".

This project was created at NSIDC so that projects across the organization could have a consistent warning message.

## Dependencies

* `jQuery 1.x` - this project uses a jQuery plugin to wait until an element is added to the page before executing a function, allowing the script to be added to your page before the desired element exists. The plugin is included in the minified script, so your project just needs to include jQuery 1.x; jQuery 2.x is not supported in IE8.

## Usage

Add the [minified script](https://bitbucket.org/nsidc/ie-warn/raw/master/build/ie-warn-latest.min.js) into your project, and load it in your header.

`<script type="text/javascript src="/path/to/ie-warn.min.js" />`

Alternatively, access the latest script directly from bitbucket rather than adding it to your project. This ensures your app always loads the latest version.

`<script type="text/javascript src="https://bitbucket.org/nsidc/ie-warn/raw/master/build/ie-warn-latest.min.js" />`

In your html, add the class "ie-warn" to whichever element you want to contain the warning.

`<div id="example" class="ie-warn"/>`

In IE8 (and earlier versions), this will cause the div "example" to display the message, "For optimum Web performance, use IE9 or later. We no longer support IE8. The use of IE8 may result in the Web page not rendering properly or the tool not functioning as expected."

The warning message can be further styled however you like using CSS specific to your project.

## Development

* `grunt jshint` runs JSHint on all of the JavaScript files in `src/`
* `grunt uglify` concatenates and minifies the files in `src/contrib/` and `src/` into `build/ie-warn-VERSION.min.js`
* `grunt copy` copies the build script of the current version to a file named `build/ie-warn-latest.min.js`
* `grunt` runs each of the other grunt tasks in the order presented here

To test your app in your `localhost` from a machine that does not have IE8, test your app in [SauceLabs](https://saucelabs.com/) with [Sauce Connect](https://saucelabs.com/docs/connect).

When changes are made (such as the message text changing, or some options being added if we ever want to use a graphic or something), be sure to increment the version number in `package.json` and run `grunt`. This will update `build/ie-warn-latest.min.js` and produce a new `build/ie-warn-VERSION.min.js`.
