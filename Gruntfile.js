module.exports = function (grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    copy: {
      main: {
        src: 'build/ie-warn-<%= pkg.version %>.min.js',
        dest: 'build/ie-warn-latest.min.js'
      }
    },

    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      all: ['Gruntfile.js', 'src/**/*.js']
    },

    uglify: {
      main: {
        files: {
          'build/ie-warn-<%= pkg.version %>.min.js': [
            'src/contrib/**/*.js',
            'src/*.js'
          ]
        }
      }
    }

  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', ['jshint', 'uglify', 'copy']);
};
